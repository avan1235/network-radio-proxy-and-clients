#ifndef RADIO_PROXY_SIGNAL_HANDLERS_H
#define RADIO_PROXY_SIGNAL_HANDLERS_H

#include <csignal>
#include <algorithm>
#include <mutex>

#include "util.h"

/**
 * Check if already INT signal was sent to process
 * @return true if signal sent, otherwise false
 */
bool got_int_signal();

/**
 * Handle INT signal by setting proper flag which is served
 * via got_int_signal() function
 * @param sig to be handled
 */
void handle_sigint(int sig);

/**
 * Set the signal handler for specified sig
 * number with sa_flags set to 0
 * @param sig to be handled
 * @param handler to be called
 */
void signal_on(int sig, void (*handler)(int));

#endif //RADIO_PROXY_SIGNAL_HANDLERS_H
