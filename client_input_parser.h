#ifndef RADIO_PROXY_CLIENT_INPUT_PARSER_H
#define RADIO_PROXY_CLIENT_INPUT_PARSER_H

#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <cstdio>

#include "util.h"

#define CLIENT_DEFAULT_TIMEOUT_S 5

struct parsed_elements {
    char *host;
    char *telnet_port;
    char *proxy_port;
    size_t timeout;
};

typedef struct parsed_elements parsed_elements_t;

/**
 * Print usage to STDERR when invalid options received
 * and debugging is enabled
 */
void print_usage();

/**
 * Parse data given by user in program argv
 * @param argc number of argv given
 * @param argv array with data from user
 * @param parsed pointer to structure to save valid data
 * @return true on valid input parameter otherwise false
 */
bool parse_elements(int argc, char **argv, parsed_elements_t *parsed);

#endif //RADIO_PROXY_CLIENT_INPUT_PARSER_H
