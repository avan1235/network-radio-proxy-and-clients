#ifndef RADIO_PROXY_UTIL_H
#define RADIO_PROXY_UTIL_H

#include <cstdio>
#include <cctype>
#include <cstdlib>
#include <cerrno>
#include <netdb.h>
#include <unordered_map>
#include <map>
#include <mutex>
#include <unistd.h>
#include <string>

#include "err.h"
#include "constants.h"
#include "signal_handlers.h"

#define DEBUG false

#define DEBUG_PRINT(data...) do { if (DEBUG) { fprintf(stderr, data); } } while (false)
#define DEBUG_PRINT_CHAR(c) DEBUG_PRINT("%c", c)
#define DEBUG_PRINT_STRING(s) DEBUG_PRINT("%s", s)

#define CHECK_ZERO_FATAL(check, msg) do { if ((check) != 0) { fatal(msg); } } while (false)
#define CHECK_FATAL(check, msg) do { if (!(check)) { fatal(msg); } } while (false)
#define CHECK_NEG_FATAL(check, msg) do { if ((check) < 0) { fatal(msg); } } while (false)
#define CHECK_ZERO_SYSERR(check, msg) do { if ((check) != 0) { syserr(msg); } } while (false)
#define CHECK_SYSERR(check, msg) do { if (!(check)) { syserr(msg); } } while (false)
#define CHECK_NEG_SYSERR(check, msg) do { if ((check) < 0) { syserr(msg); } } while (false)

#define MIN_TIMEOUT_S 1

#define MIN_PORT 1
#define MAX_PORT 65535

/**
 * Compare if pre matches as a prefix of str
 * @param pre string to be matched
 * @param str to be searched in
 * @return true on match otherwise false
 */
bool prefix_cmp(const char *pre, const char *str);

/**
 * Compare if pre matches as a prefix of str ignoring casing
 * @param pre string to be matched
 * @param str to be searched in
 * @return true on match otherwise false
 */
bool prefix_cmp_case(const char *pre, const char *str);

/**
 * Check if specified position in buffer is CRLF
 * @param buffer position to be checked
 * @return true on match otherwise false
 */
bool is_line_end(const char *buffer);

/**
 * Print char to specified stream
 * @param c to be printed
 * @param stream to print data to
 */
void print_char(char c, FILE *stream);

/**
 * Print buffer content to specified stream (may be stdout, stderr)
 * @param buf data to be printed
 * @param read_len max position in buf
 * @param start position in buf to start printing
 * @param stream pointer to stream to print the data
 */
void print_buffer(char *buf, size_t read_len, size_t start, FILE *stream);

/**
 * Convert number from string to size_t value
 * @param string_number to be parsed
 * @param result pointer when the result is saved if valid
 * @return true when parsed with no errors otherwise false
 */
bool string_to_uint(const char *string_number, size_t *result);

/**
 * Get the address specification for application socked based
 * on input parameters as the hints to get proper address
 * @param address of the interface to get the address of
 * @param port on the interface to get the address on
 * @param addr_result where the result will be stored
 * @return on valid address parsing process otherwise false
 */
bool tcp_addrrinfo(char *address, char *port, addrinfo **addr_result);

/**
 * Check if specified timeout has valid value
 * @param string to be parsed
 * @param timeout where valid result is saved
 * @return true if valid otherwise false
 */
bool parse_timeout(char *string, size_t *timeout);

/**
 * Check if string represents valid radio_port value
 * @param string to be parsed
 * @return true if valid otherwise false
 */
bool is_valid_port(char *string);

/**
 * Read new data from socket and validate if read was successfully finished
 * @param buffer pointer to begin of buffer memory
 * @param bytes number of bytes to be read from socket to buffer
 * @param sock to read from
 * @return number of bytes read from socket to buffer
 */
size_t read_data(char *buffer, size_t bytes, int sock);

/**
 * Write data to socket and check writing result - raise system error
 * on write problems
 * @param sock to write data to
 * @param bytes to send count
 * @param data to be sent to socket
 */
void write_checked(const char *data, size_t bytes, int sock);

/**
 * Get address equal to INADDR_ANY with specified parsed
 * port in sockaddr_in structure specified
 * @param port to be parsed into result
 * @param addr_result where result is saved
 * @return true on success otherwise false
 */
bool get_local_addr(char *port, sockaddr_in *addr_result);

/**
 * Convert buffer of data from network to host order of bytes
 * @param buffer to be converted
 * @param len of buffer in indexes
 */
void ntoh_buff(uint16_t *buffer, size_t len);

/**
 * Convert buffer of data from host to network order of bytes
 * @param buffer to be converted
 * @param len of buffer in indexes
 */
void hton_buff(uint16_t *buffer, size_t len);

/**
 * Get current time in seconds and nanos.
 * On failing exits with SYSERR as it is always
 * fatal error when clock is not available
 * @param result where the time is stored
 */
void gettime(timespec *result);

/**
 * Check if specified las time fits in range of
 * seconds where the current time measure is done
 * as a part of this function
 * @param last_time to be compared with current time
 * @param range in which time difference should fit in seconds
 * @return true if fits otherwise false (or exists SYSERR on clock error)
 */
bool fits_time_range(const timespec &last_time, size_t range);

/**
 * Compare sockaddr_in structures by comparing their address and ports
 */
struct sockaddr_in_eq
{
    bool operator()(const sockaddr_in& k1, const sockaddr_in& k2) const noexcept
    {
        return k1.sin_addr.s_addr == k2.sin_addr.s_addr && k1.sin_port == k2.sin_port;
    }
};

/**
 * Compare sockaddr_in structures by comparing their address and ports
 */
struct sockaddr_in_cmp
{
    bool operator()(const sockaddr_in& k1, const sockaddr_in& k2) const noexcept
    {
        return k1.sin_addr.s_addr == k2.sin_addr.s_addr
               ? k1.sin_port < k2.sin_port
               : k1.sin_addr.s_addr <= k2.sin_addr.s_addr;
    }
};

typedef std::map<sockaddr_in, timespec, sockaddr_in_cmp> clients_map;

typedef std::map<sockaddr_in, std::string, sockaddr_in_cmp> servers_map;

#endif //RADIO_PROXY_UTIL_H
