CC = g++
CFLAGS = -std=c++17 -Wall -Wextra -O2 -lpthread
PROXY_SRC = err.cpp util.cpp proxy_input_parser.cpp signal_handlers.cpp request.cpp radio_parser.cpp radio_proxy.cpp
CLIENT_SRC = err.cpp client_input_parser.cpp util.cpp signal_handlers.cpp telnet.cpp radio_client.cpp
RM = rm -f

all: radio-proxy radio-client

%.o: %.cpp
	$(CC) $(CFLAGS) -c -o $@ $<

radio-proxy: $(PROXY_SRC:%.cpp=%.o)
	$(CC) $(CFLAGS) -o $@ $^

radio-client: $(CLIENT_SRC:%.cpp=%.o)
	$(CC) $(CFLAGS) -o $@ $^

clean: compile-clean
	@$(RM) radio-proxy radio-client

compile-clean:
	@$(RM) *.o