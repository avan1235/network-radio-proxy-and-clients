#include "request.h"

/**
 * Get meta value for request  parameter in specified format
 * 1 for requesting the metadata, 0 for not requesting metadata
 * @param meta if metadata is requested by user
 * @return string for request
 */
static inline std::string get_meta_value(bool meta)
{
    return meta ? "1" : "0";
}

void send_request(int sock, parsed_elements_t *parsed)
{
    DEBUG_PRINT("Sending request:\n");

    std::string request_elements[REQUEST_ELEMENTS] = {
            GET, parsed->resource, HTTP_1_0, CRLF,
            HOST_KEY, parsed->host, CRLF,
            USER_AGENT_KEY, USER_AGENT_VALUE, CRLF,
            METADATA_KEY, get_meta_value(parsed->meta), CRLF,
            CRLF
    };

    for (auto & request_element : request_elements) {
        write_checked(request_element.c_str(), strlen(request_element.c_str()), sock);
        DEBUG_PRINT_STRING(request_element.c_str());
    }
}