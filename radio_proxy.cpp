#include <thread>

#include "proxy_input_parser.h"
#include "radio_parser.h"
#include "util.h"
#include "request.h"
#include "constants.h"
#include "signal_handlers.h"
#include "err.h"

int main (int argc, char **argv)
{
    int radio_sock, server_sock;
    parsed_elements_t parsed;
    addrinfo *addr_result = nullptr;
    ip_mreq ip_mreq{};
    sockaddr_in local_address{};
    char buffer[RESPONSE_BUFFER_SIZE + 1];
    char radio_name[NAME_BUFFER_SIZE + 1] = DEFAULT_RADIO_NAME;
    size_t buf_pos, read_len, meta_int;
    bool as_proxy;

    if (!parse_elements(argc, argv, &parsed)) {
        print_usage();
        return EXIT_FAILURE;
    }
    as_proxy = parsed.listen_port != nullptr;

    DEBUG_PRINT("Input configuration:\n"
                "host: %s, radio_port: %s, resource: %s, meta: %d, timeout: %zu, "
                "Port: %s, Broadcast: %s, Timeout: %zu\n",
                parsed.host, parsed.radio_port, parsed.resource, parsed.meta, parsed.timeout,
                parsed.listen_port, parsed.group_address, parsed.client_timeout);

    signal_on(SIGINT, handle_sigint);
    CHECK_NEG_SYSERR(radio_sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP), "socket radio");

    timeval time_out = { .tv_sec = (long int) parsed.timeout, .tv_usec = 0 };
    CHECK_ZERO_SYSERR(setsockopt(radio_sock, SOL_SOCKET, SO_RCVTIMEO, &time_out,
                                 sizeof(timeval)), "setsockopt");

    CHECK_SYSERR(tcp_addrrinfo(parsed.host, parsed.radio_port, &addr_result), "get_addr");
    CHECK_ZERO_SYSERR(connect(radio_sock, addr_result->ai_addr, addr_result->ai_addrlen), "connect radio");

    freeaddrinfo(addr_result);
    addr_result = nullptr;

    send_request(radio_sock, &parsed);

    buf_pos = 0;
    read_len = read_data(buffer, RESPONSE_BUFFER_SIZE, radio_sock);
    if (!parse_status_line(buffer, read_len, &buf_pos)) {
        exit(EXIT_FAILURE);
    }

    meta_int = 0;
    if (read_header(buffer, &read_len, &buf_pos, &meta_int, radio_name, radio_sock)) {
        DEBUG_PRINT("Header parsed meta: %zu\n", meta_int);
        CHECK_FATAL(parsed.meta, "Server send data with meta when not requested");
    }
    else {
        parsed.meta = false;
    }

    DEBUG_PRINT("Header parsed radio name: %s\n", radio_name);

    if (as_proxy) {
        DEBUG_PRINT("Binding as proxy server\n");
        CHECK_NEG_SYSERR(server_sock = socket(AF_INET, SOCK_DGRAM, 0), "socket proxy");

        timeval client_time_out = { .tv_sec = (long int) parsed.client_timeout, .tv_usec = 0 };
        CHECK_ZERO_SYSERR(setsockopt(server_sock, SOL_SOCKET, SO_RCVTIMEO, &client_time_out,
                                     sizeof(timeval)), "setsockopt timeout");

        if (parsed.group_address != nullptr) {
            ip_mreq.imr_interface.s_addr = htonl(INADDR_ANY);
            CHECK_FATAL(inet_aton(parsed.group_address, &ip_mreq.imr_multiaddr), "inet_aton");
            CHECK_ZERO_SYSERR(setsockopt(server_sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &ip_mreq,
                                         sizeof(ip_mreq)), "setsockopt membership");
        }

        CHECK_FATAL(get_local_addr(parsed.listen_port, &local_address), "get_local_addr");
        CHECK_ZERO_SYSERR(bind(server_sock, (sockaddr *) &local_address,
                               sizeof(sockaddr_in)), "bind");

        proxy_t params {
            .proxy_sock = server_sock,
            .radio_sock = radio_sock,
            .clients_timeout = parsed.client_timeout,
            .radio_name = radio_name,
            .clients_times{},
            .mutex{}
        };
        std::thread info_thread(handle_connection, &params);
        serve_response_body(buffer, read_len, buf_pos, parsed.meta, meta_int, params);
        DEBUG_PRINT("Wait for handle_connection to finish\n");
        info_thread.join();

        DEBUG_PRINT("Close proxy server\n");
        if (parsed.group_address != nullptr) {
            CHECK_ZERO_SYSERR(setsockopt(server_sock, IPPROTO_IP, IP_DROP_MEMBERSHIP,
                    &ip_mreq, sizeof(ip_mreq)), "setsockopt");
        }
        CHECK_ZERO_SYSERR(close(server_sock), "close");
    }
    else {
        local_read_response_body(buffer, read_len, buf_pos, parsed.meta, meta_int, stdout, stderr, radio_sock);
    }

    CHECK_ZERO_SYSERR(close(radio_sock), "close");
    DEBUG_PRINT("Connection closed\n");

    return EXIT_SUCCESS;
}
