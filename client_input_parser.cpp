#include "client_input_parser.h"

bool parse_elements(int argc, char **argv, parsed_elements_t *parsed)
{
    int c;
    char *timeout = nullptr;
    memset(parsed, 0, sizeof(parsed_elements_t));

    while ((c = getopt(argc, argv, "H:P:p:T:")) != -1) {
        switch (c)
        {
            case 'H': parsed->host = optarg; break;
            case 'P': parsed->proxy_port = optarg; break;
            case 'p': parsed->telnet_port = optarg; break;
            case 'T': timeout = optarg; break;
            case '?':
            default:
                return false;
        }
    }

    if (parsed->host == nullptr || parsed->proxy_port == nullptr
        || parsed->telnet_port == nullptr) {
        return false;
    }

    if (timeout == nullptr) {
        parsed->timeout = CLIENT_DEFAULT_TIMEOUT_S;
    }
    else if (!parse_timeout(timeout, &parsed->timeout)) {
        return false;
    }

    return is_valid_port(parsed->telnet_port) && is_valid_port(parsed->proxy_port);
}

void print_usage()
{
    DEBUG_PRINT(
            "Options: \n"
            "-H host [required] - host for radio-proxy\n"
            "-P radio_port [required] - radio_port on which radio-proxy listen\n"
            "-p radio_port [required] - radio_port to connect as telnet client\n"
            "-T timeout [optional default=%d] - radio-proxy IAM timeout seconds\n",
            CLIENT_DEFAULT_TIMEOUT_S);
}