#ifndef RADIO_PROXY_ERR_H
#define RADIO_PROXY_ERR_H

#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <cstring>

/**
 * Get information about failure system function call
 * and exit program with EXIT_FAILURE code
 * @param fmt format to be printed as errors
 * @param ... printed data
 */
extern void syserr(const char *fmt, ...);

/**
 * Print message about the error and exit program with
 * EXIT_FAILURE code
 * @param fmt message to be printed
 * @param ... printed data
 */
extern void fatal(const char *fmt, ...);

#endif //RADIO_PROXY_ERR_H

