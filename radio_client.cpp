#include <thread>

#include "client_input_parser.h"
#include "signal_handlers.h"
#include "constants.h"
#include "util.h"
#include "telnet.h"

int main (int argc, char **argv)
{
    int telnet_sock, proxy_sock, opt;
    parsed_elements_t parsed;
    sockaddr_in telnet_addr{};
    sockaddr_in proxy_addr{};
    servers_map servers{};
    auto playing = servers.cend();

    if (!parse_elements(argc, argv, &parsed)) {
        print_usage();
        return EXIT_FAILURE;
    }

    DEBUG_PRINT("Input configuration:\n"
                "Host: %s, Port: %s, radio_port: %s, Timeout: %zu\n",
                parsed.host, parsed.proxy_port, parsed.telnet_port, parsed.timeout);

    signal_on(SIGINT, handle_sigint);
    CHECK_NEG_SYSERR(telnet_sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP), "socket");
    CHECK_NEG_SYSERR(proxy_sock = socket(AF_INET, SOCK_DGRAM, 0), "socket");

    opt = 1;
    CHECK_ZERO_SYSERR(setsockopt(proxy_sock, SOL_SOCKET, SO_BROADCAST,
            &opt, sizeof(opt)), "setsockopt broadcast");
    opt = MAX_TTL_CLIENT;
    CHECK_ZERO_SYSERR(setsockopt(proxy_sock, IPPROTO_IP, IP_MULTICAST_TTL,
            &opt, sizeof opt), "setsockopt ttl");
    opt = 1;
    CHECK_ZERO_SYSERR(setsockopt(proxy_sock, IPPROTO_IP, IP_MULTICAST_LOOP,
            &opt, sizeof(opt)), "setsockopt loop");

    timeval proxy_time_out = { .tv_sec = RECV_TIMEOUT_S, .tv_usec = 0 };
    CHECK_ZERO_SYSERR(setsockopt(proxy_sock, SOL_SOCKET, SO_RCVTIMEO, &proxy_time_out,
                                 sizeof(timeval)), "setsockopt loop");
    CHECK_FATAL(get_local_addr(parsed.proxy_port, &proxy_addr), "get_local_addr");
    CHECK_FATAL(inet_aton(parsed.host, &proxy_addr.sin_addr), "inet_aton");
    CHECK_FATAL(get_local_addr(parsed.telnet_port, &telnet_addr), "get_local_addr");
    CHECK_ZERO_SYSERR(bind(telnet_sock, (sockaddr *) &telnet_addr, sizeof(sockaddr_in)), "bind");
    CHECK_ZERO_SYSERR(listen(telnet_sock, MAX_TELNET_QUEUE), "listen");

    telnet_t params = {
            .servers = servers,
            .servers_mutex{},
            .menu_pos = 0,
            .selected = playing,
            .telnet_mutex{},
            .current_title = EMPTY_TRACK_TITLE,
            .finished = false,
            .msg_sock = 0,
            .telnet_sock = telnet_sock,
            .proxy_sock = proxy_sock,
            .timeout = parsed.timeout,
            .proxy_addr = proxy_addr,
            .last_conn{},
    };

    std::thread alive_thread(keep_alive, &params);
    std::thread receive_thread(receive_proxy_data, &params);
    std::thread check_thread(check_alive_current, &params);
    handle_telnet_connections(params);

    DEBUG_PRINT("Wait for check_alive_current to finish\n");
    check_thread.join();
    DEBUG_PRINT("Wait for receive_proxy_data to finish\n");
    receive_thread.join();
    DEBUG_PRINT("Wait for keep_alive to finish\n");
    alive_thread.join();

    CHECK_ZERO_SYSERR(close(proxy_sock), "close");
    CHECK_ZERO_SYSERR(close(telnet_sock), "close");
    DEBUG_PRINT("Connections closed\n");

    return EXIT_SUCCESS;
}