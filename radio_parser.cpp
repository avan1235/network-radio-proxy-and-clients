#include "radio_parser.h"

/**
 * Shift left content of buffer by specified number of bytes
 * and set the rest of the buffer bytes to zeros and fix
 * the available data len in buffer
 * @param buf buffer with data to be shifted
 * @param by_bytes how many bytes shift data to left
 * @param read_len pointer to read data in buffer to be fixed after shifting
 */
static void shl_buf(char *buf, size_t by_bytes, size_t *read_len);

/**
 * Read the data from buffer to CRLF and do nothing with it
 * @param buf with data line
 * @param pos current position in buffer to be incremented
 * @param max_buf_pos pox position in buffer when incrementing
 */
static void simple_read_to_eol(char *buf, size_t *pos, size_t max_buf_pos);

/**
 * Read data from buffer to CRLF and if specified line didn't fit
 * into buffer then try to read it again
 * @param buf with data to be parsed
 * @param read_len of data read into buffer
 * @param buf_pos current position in buffer
 * @param sock to read data from when not full line loaded
 */
static void read_to_eol(char *buf, size_t *read_len, size_t *buf_pos, int sock);

/**
 * Parse header value with length of not metadata content in body and shift
 * the buf pos to the next line in header buffer
 * @param buf with data to be parsed
 * @param read_len of data read into buffer
 * @param buf_pos current position in buffer
 * @return value parsed from meta header
 */
static size_t parse_meta_value(char *buf, const size_t *read_len, size_t *buf_pos);

/**
 * Parse header value with name of radio served and copy this name ot buffer
 * (as many bytes as will fit in it) and shift the buf pos to the next line in header
 * buffer
 * @param buf with data to be parsed
 * @param read_len of data read into buffer
 * @param buf_pos current position in buffer
 * @param name_buf where the parsed data will be stored (of max size NAME_BUFFER_SIZE)
 * @param sock to read data from if name is longer than name buffer size so is not fully
 * copied to the buffer name
 */
static void parse_name_value(char *buf, size_t *read_len, size_t *buf_pos, char *name_buf, int sock);

/**
 * Print buffer content to specified stream and read more from socket wen needed
 * @param buf with data to be printed
 * @param read_len of data read into buffer
 * @param buf_pos current position in buffer
 * @param to_print count of bytes to print from buffer
 * @param stream to print data to
 * @param sock to read data from
 * @return number of last read_data operation on socket
 */
static size_t print_single_stream(char *buf, size_t *read_len, size_t *pos, size_t to_print, FILE *stream, int sock);

/**
 * Send data to all connected clients with specified type of communicate
 * @param buf_pos position in buffer to start sending data
 * @param write_len length of data to be send (buffer has to have at least write_len data)
 * @param type of communicate to send in header (AUDIO or METADATA)
 * @param params with proxy configuration
 */
static void send_to_all_clients(const char *buf_pos, size_t write_len, uint16_t type, proxy_t &params);

/**
 * Send data to all connected clients and take care of data send size
 * not to be bigger than MAX_SEND_DATA_LEN
 * @param buf to send data from
 * @param max_pos in buffer (so the data loaded to buffer size)
 * @param pos to start sending from the data
 * @param type of communicate to send
 * @param params with proxy configuration
 */
static void send_buffer(char *buf, size_t max_pos, size_t pos, uint16_t type, proxy_t &params);

/**
 * Send single stream of data (AUDIO or METADATA) by loading it from radio
 * socket and the sending to all connected clients
 * @param buf where the init data is loaded
 * @param read_len of data loaded to buffer
 * @param pos to start reading from buffer
 * @param to_send_count number of bytes to be loaded/sent before changing type of communicate
 * @param type of communicate to send
 * @param params with proxy configuration
 * @return number of byte loaded last tie from radio (as we should stop reading when 0 received)
 */
static size_t send_stream(char *buf, size_t *read_len, size_t *pos, size_t to_send_count, uint16_t type, proxy_t &params);

/**
 * Read data from radio and send it to all cliets using the given functions
 * as the callback for consuming read data
 * @param buf with already loaded data
 * @param read_len of data loaded to buffer
 * @param pos to start reading from buffer
 * @param has_meta treu when server is going to serve also metadata info
 * @param meta_int length of data between metadata blocks when has_meta is true
 * @param radio_sock to read data from radio
 * @param send_data_buffer function to send whole data to clients when no metadata comes from buffer
 * @param send_read_audio_stream function to send AUDIO data to clients
 * @param send_read_meta_stream function to send METADATA data to clients
 */
static void serve_unified_response_body(char *buf, size_t read_len, size_t pos,
                                        bool has_meta, size_t meta_int, int radio_sock,
                                        const std::function<void(char *, size_t, size_t)> &send_data_buffer,
                                        const std::function<size_t(char *, size_t *, size_t *, size_t)> &send_read_audio_stream,
                                        const std::function<size_t(char *, size_t *, size_t *, size_t)> &send_read_meta_stream);

bool parse_status_line(const char *buf, size_t read_len, size_t *buf_pos)
{
    if (read_len < MIN_VALID_STATUS_LINE_LEN) {
        return false;
    }

    static const char *valid_status_lines[POSSIBLE_VALID_STATUS_LINES] = {
            "ICY 200 OK" CRLF,
            "HTTP/1.0 200 OK" CRLF,
            "HTTP/1.1 200 OK" CRLF
    };

    for (auto & valid_status_line : valid_status_lines) {
        if (prefix_cmp(valid_status_line, buf)) {
            *buf_pos += strlen(valid_status_line);
            return true;
        }
    }

    return false;
}

bool read_header(char *buf, size_t *read_len, size_t *buf_pos, size_t *meta_int, char *radio_name, int sock)
{
    bool found_crlf = false;
    bool found_meta_int = false;
    while (!found_crlf) {
        found_crlf = is_line_end(buf + *buf_pos);
        if (!found_crlf) {
            if (*read_len - *buf_pos < ICY_HEADER_DATA_MAX_LEN) {
                shl_buf(buf, *buf_pos, read_len);
                *buf_pos = 0;
                *read_len += read_data(buf + *read_len, RESPONSE_BUFFER_SIZE - *read_len, sock);
            }
            if (prefix_cmp_case(META_INT_KEY, buf + *buf_pos)) {
                DEBUG_PRINT(META_INT_KEY);
                *meta_int = parse_meta_value(buf, read_len, buf_pos);
                found_meta_int = true;
            }
            else if (prefix_cmp_case(META_NAME_KEY, buf + *buf_pos)) {
                DEBUG_PRINT(META_NAME_KEY);
                parse_name_value(buf, read_len, buf_pos, radio_name, sock);
            }
            else {
                read_to_eol(buf, read_len, buf_pos, sock);
            }
        }
    }
    *buf_pos += CRLF_LEN;
    return found_meta_int;
}

void local_read_response_body(char *buf, size_t read_len, size_t pos, bool has_meta, size_t meta_int,
                              FILE *data_stream, FILE *meta_stream, int sock)
{
    const auto send_data_buffer = [data_stream](char *buf, size_t read_len, size_t pos) {
        print_buffer(buf, read_len, pos, data_stream);
    };
    const auto send_audio_stream = [data_stream, sock](char *buf, size_t *read_len, size_t *pos, size_t count) {
        return print_single_stream(buf, read_len, pos, count, data_stream, sock);
    };
    const auto send_meta_stream = [meta_stream, sock](char *buf, size_t *read_len, size_t *pos, size_t count) {
        return print_single_stream(buf, read_len, pos, count, meta_stream, sock);
    };
    serve_unified_response_body(buf, read_len, pos, has_meta, meta_int, sock,
            send_data_buffer, send_audio_stream, send_meta_stream);
}

void handle_connection(proxy_t *params)
{
    char *radio_name = params->radio_name;
    int server_sock = params->proxy_sock;
    clients_map &clients_times = params->clients_times;

    sockaddr_in client_address{};
    auto rcva_len = (socklen_t) sizeof(client_address);
    ssize_t len;
    uint16_t name_len = strlen(radio_name);
    size_t message_size_bytes = PROXY_HEADER_SIZE + name_len;
    uint16_t received[PROXY_HEADER_FIELDS];
    uint8_t sending[message_size_bytes];

    auto *header = (uint16_t *) sending;
    header[PROXY_HEADER_FIELD_TYPE] = IAM;
    header[PROXY_HEADER_FIELD_LENGTH] = name_len;
    hton_buff(header, PROXY_HEADER_FIELDS);
    auto *body = (uint8_t *) (header + PROXY_HEADER_FIELDS);
    for (size_t i = 0; i < name_len; ++i) {
        body[i] = radio_name[i];
    }

    DEBUG_PRINT("Handle input connections for: %s\n", radio_name);
    while (!got_int_signal()) {
        len = recvfrom(server_sock, received, sizeof(received), 0,
                       (sockaddr *) &client_address, &rcva_len);
        if (len != PROXY_HEADER_SIZE) {
            DEBUG_PRINT("Got message of invalid length %ld. Skipping\n", len);
            continue;
        }
        ntoh_buff(received, PROXY_HEADER_FIELDS);
        if (received[PROXY_HEADER_FIELD_LENGTH] != 0) {
            DEBUG_PRINT("Invalid header field length from %s:%d\n",
                    inet_ntoa(client_address.sin_addr),
                    client_address.sin_port);
            continue;
        }
        DEBUG_PRINT("Got %d from %s:%d\n", received[PROXY_HEADER_FIELD_TYPE],
                    inet_ntoa(client_address.sin_addr),
                    client_address.sin_port);
        timespec time{};
        gettime(&time);
        if (received[PROXY_HEADER_FIELD_TYPE] == DISCOVER) {
            DEBUG_PRINT("Received DISCOVER from %s:%d\n",
                        inet_ntoa(client_address.sin_addr),
                        client_address.sin_port);
            std::scoped_lock lock(params->mutex);
            clients_times[client_address] = time;
            sendto(server_sock, sending, sizeof(sending), 0,
                   (sockaddr *) &client_address, rcva_len);
        }
        else if (received[PROXY_HEADER_FIELD_TYPE] == KEEPALIVE) {
            DEBUG_PRINT("Received KEEPALIVE from %s:%d\n",
                    inet_ntoa(client_address.sin_addr),
                    client_address.sin_port);
            DEBUG_PRINT("Wait for servers_mutex to access client data\n");
            std::scoped_lock lock(params->mutex);
            auto it = clients_times.find(client_address);
            if (it != clients_times.end()) {
                it->second = time;
            }
            DEBUG_PRINT("After update of time the number of clients is %zu\n", clients_times.size());
            DEBUG_PRINT("Time data updated\n");
        }
        else {
            DEBUG_PRINT("Got not supported field type\n");
        }
    }
    DEBUG_PRINT("Finished handle_connection\n");
}

void serve_response_body(char *buf, size_t read_len, size_t pos, bool has_meta, size_t meta_int, proxy_t &params)
{
    const auto send_data_buffer = [&params](char *buf, size_t read_len, size_t pos) {
        send_buffer(buf, read_len, pos, AUDIO, params);
    };
    const auto send_audio_stream = [&params](char *buf, size_t *read_len, size_t *pos, size_t count) {
        return send_stream(buf, read_len, pos, count, AUDIO, params);
    };
    const auto send_meta_stream = [&params](char *buf, size_t *read_len, size_t *pos, size_t count) {
        return send_stream(buf, read_len, pos, count, METADATA, params);
    };
    serve_unified_response_body(buf, read_len, pos, has_meta, meta_int, params.radio_sock,
                                send_data_buffer, send_audio_stream, send_meta_stream);
}

static void serve_unified_response_body(char *buf, size_t read_len, size_t pos,
        bool has_meta, size_t meta_int, int radio_sock,
        const std::function<void(char *, size_t, size_t)> &send_data_buffer,
        const std::function<size_t(char *, size_t *, size_t *, size_t)> &send_read_audio_stream,
        const std::function<size_t(char *, size_t *, size_t *, size_t)> &send_read_meta_stream)
{
    if (!has_meta) {
        send_data_buffer(buf, read_len, pos);
        while ((read_len = read_data(buf, RESPONSE_BUFFER_SIZE, radio_sock)) != 0) {
            send_data_buffer(buf, read_len, 0);
        }
    }
    else {
        size_t meta_len;
        while (read_len != 0) {
            DEBUG_PRINT("Sending AUDIO of length %zu\n", meta_int);
            if (send_read_audio_stream(buf, &read_len, &pos, meta_int) == 0) {
                break;
            }

            if (pos == read_len) {
                if ((read_len = read_data(buf, RESPONSE_BUFFER_SIZE, radio_sock)) == 0) {
                    break;
                }
                pos = 0;
            }
            meta_len = META_INT_MULTIPLY * buf[pos];
            pos += 1;
            DEBUG_PRINT("Sending METADATA of length %zu\n", meta_len);
            if (send_read_meta_stream(buf, &read_len, &pos, meta_len) == 0) {
                break;
            }
        }
    }
}

static void send_to_all_clients(const char *buf_pos, size_t write_len, uint16_t type, proxy_t &params)
{
    size_t message_size_bytes = PROXY_HEADER_SIZE + write_len;
    uint8_t sending[message_size_bytes];
    auto *header = (uint16_t *) sending;
    header[PROXY_HEADER_FIELD_TYPE] = type;
    header[PROXY_HEADER_FIELD_LENGTH] = write_len;
    hton_buff(header, PROXY_HEADER_FIELDS);
    auto *body = (uint8_t *) (header + PROXY_HEADER_FIELDS);
    for (size_t i = 0; i < write_len; ++i) {
        body[i] = buf_pos[i];
    }

    std::scoped_lock lock(params.mutex);
    auto it = params.clients_times.begin();
    while (it != params.clients_times.end() && !got_int_signal()) {
        if (fits_time_range(it->second, params.clients_timeout)) {
            auto client = (sockaddr *) &it->first;
            sendto(params.proxy_sock, sending, sizeof(sending), 0, client, sizeof(sockaddr_in));
            ++it;
        }
        else {
            DEBUG_PRINT("Client time out of range - removing from list %s:%d\n",
                    inet_ntoa(it->first.sin_addr),
                    it->first.sin_port);
            it = params.clients_times.erase(it);
        }
    }
}

static void send_buffer(char *buf, size_t max_pos, size_t pos, uint16_t type, proxy_t &params)
{

    size_t to_send = max_pos - pos;
    while (to_send > 0 && !got_int_signal()) {
        size_t write_len = std::min(to_send, (size_t) MAX_SEND_DATA_LEN);
        send_to_all_clients(buf + pos, write_len, type, params);
        pos += write_len;
        to_send = max_pos - pos;
    }
}

static size_t send_stream(char *buf, size_t *read_len, size_t *pos, size_t to_send_count, uint16_t type, proxy_t &params)
{
    while (to_send_count > 0 && !got_int_signal())
    {
        size_t in_buff = *read_len - *pos;
        if (in_buff >= to_send_count) {
            send_buffer(buf, *pos + to_send_count, *pos, type, params);
            *pos += to_send_count;
            to_send_count = 0;
        }
        else { // in_buff < to_send_count
            send_buffer(buf, *read_len, *pos, type, params);
            to_send_count -= in_buff;
            *pos = 0;
            *read_len = read_data(buf, RESPONSE_BUFFER_SIZE, params.radio_sock);
            if (*read_len == 0) {
                break;
            }
        }
    }
    return *read_len;
}

static void shl_buf(char *buf, size_t by_bytes, size_t *read_len)
{
    size_t i = 0;
    size_t left_len = *read_len - by_bytes;
    while (by_bytes < *read_len) {
        buf[i++] = buf[by_bytes++];
    }
    while (i < RESPONSE_BUFFER_SIZE + 1) {
        buf[i++] = EOS;
    }
    *read_len = left_len > 0 ? left_len : 0;
}

static void simple_read_to_eol(char *buf, size_t *pos, size_t max_buf_pos)
{
    while (*pos < max_buf_pos && !is_line_end(buf + *pos)) {
        DEBUG_PRINT_CHAR(buf[*pos]);
        *pos += 1;
    }
}

static void read_to_eol(char *buf, size_t *read_len, size_t *buf_pos, int sock)
{
    size_t pos = *buf_pos;
    size_t max_buf_pos = *read_len;
    simple_read_to_eol(buf, &pos, max_buf_pos);
    if (pos == max_buf_pos) { // read again to try to fit the whole line into buffer
        max_buf_pos = read_data(buf, RESPONSE_BUFFER_SIZE, sock);
        pos = 0;
        simple_read_to_eol(buf, &pos, max_buf_pos);
        if (pos == max_buf_pos) {
            fatal("buffer too small to fit single line");
        }
    }
    DEBUG_PRINT(CRLF);
    *buf_pos = pos + CRLF_LEN;
    *read_len = max_buf_pos;
}

static size_t parse_meta_value(char *buf, const size_t *read_len, size_t *buf_pos)
{
    *buf_pos += META_INT_KEY_LEN;
    if (buf[*buf_pos] == SP) {
        DEBUG_PRINT_CHAR(SP);
        *buf_pos += 1;
    }
    const char *value_begin = buf + *buf_pos;
    size_t pos = *buf_pos;
    size_t max_buf_pos = *read_len;
    simple_read_to_eol(buf, &pos, max_buf_pos);
    if (pos == max_buf_pos) {
        fatal("too long number in meta length value");
    }
    buf[pos] = EOS; // mark end of line as end of string for parsing
    size_t value;
    if (!string_to_uint(value_begin, &value)) {
        fatal("string to uint cannot be converted");
    }
    DEBUG_PRINT(CRLF);
    *buf_pos = pos + CRLF_LEN; // and save the position of line end
    return value;
}

static void parse_name_value(char *buf, size_t *read_len, size_t *buf_pos, char *name_buf, int sock)
{
    *buf_pos += META_NAME_KEY_LEN;
    if (buf[*buf_pos] == SP) {
        DEBUG_PRINT_CHAR(SP);
        *buf_pos += 1;
    }

    size_t i = 0;
    while (*buf_pos < *read_len && !is_line_end(buf + *buf_pos) && i < NAME_BUFFER_SIZE) {
        name_buf[i] = buf[*buf_pos];
        DEBUG_PRINT_CHAR(buf[*buf_pos]);
        *buf_pos += 1;
        i += 1;
    }
    name_buf[i] = EOS;
    if (!is_line_end(buf + *buf_pos)) {
        read_to_eol(buf, read_len, buf_pos, sock);
    }
    else {
        DEBUG_PRINT(CRLF);
        *buf_pos += CRLF_LEN;
    }
}

static size_t print_single_stream(char *buf, size_t *read_len, size_t *pos, size_t to_print, FILE *stream, int sock)
{
    while (to_print > 0 && !got_int_signal()) {
        if (*pos < *read_len) {
            print_char(buf[*pos], stream);
            *pos += 1;
            to_print -= 1;
        }
        else {
            *read_len = read_data(buf, RESPONSE_BUFFER_SIZE, sock);
            if (*read_len == 0) {
                return 0;
            }
            *pos = 0;
        }
    }
    return *read_len;
}