#ifndef RADIO_PROXY_CONSTANTS_H
#define RADIO_PROXY_CONSTANTS_H

#define RESPONSE_BUFFER_SIZE 131072
#define NAME_BUFFER_SIZE 1024
#define EOS '\0'
#define EOL '\n'
#define CR '\r'
#define SP ' '
#define CRLF "\r\n"
#define CRLF_LEN 2

#define DEFAULT_RADIO_NAME "unknown name" // must be shorter than NAME_BUFFER_SIZE
#define EMPTY_TRACK_TITLE ""

#define PROXY_HEADER_SIZE 4
#define PROXY_HEADER_FIELD_TYPE 0
#define PROXY_HEADER_FIELD_LENGTH 1
#define PROXY_HEADER_FIELDS 2 // must be equal to max field index + 1

#define MAX_SEND_DATA_LEN 65535
#define MAX_TELNET_QUEUE 16
#define MAX_TTL_CLIENT 16

#define DISCOVER 1 // client looking for available proxies
#define IAM 2 // proxy respond with the name of teh served radio
#define KEEPALIVE 3 // client sends info that it is still working
#define AUDIO 4 // proxy sends audio data
#define METADATA 6 // proxy sends meta data

#endif //RADIO_PROXY_CONSTANTS_H
