# Shoutcast Radio Proxy-Client in raw C sockets

Project for radio data receiving using shoutcast protocol. Project written for 
Network classes at the University of Warsaw.

## Compile

```shell script
make
```

## Example runs of program

### Run proxy in proxy mode
```shell script
./radio-proxy -h waw02-03.ic.smcdn.pl -r /t050-1.mp3 -p 8000 -m yes -P 54321
```
### Run proxy connected to group
```shell script
./radio-proxy -p 8000 -h waw02-03.ic.smcdn.pl -r /t043-1.mp3 -m yes -B 239.10.11.12 -P 54321
```

### Run client
```shell script
./radio-client -H 127.0.0.1 -P 54321 -p 10001 | mplayer -cache 2048 -
```

### Connect by telnet
```shell script
telnet localhost 10001
```