#include "util.h"

/**
 * Validate if specified letter is a letter
 * @param c to be checked
 * @return true if alpha given otherwise false
 */
static inline bool is_alpha(char c)
{
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

/**
 * Standardize UPPERCASE letter to lowercase
 * @param c to be converted if needed
 * @return converted letter
 */
static inline char to_lower(char c)
{
    if (c >= 'A' && c <= 'Z') {
        return c + ('a' - 'A');
    }
    else {
        return c;
    }
}

bool prefix_cmp(const char *pre, const char *str)
{
    while(*pre) {
        if(*pre++ != *str++) {
            return false;
        }
    }
    return true;
}

bool prefix_cmp_case(const char *pre, const char *str)
{
    while(*pre) {
        if((*pre != *str)
           && (!is_alpha(*pre) || !is_alpha(*str) || to_lower(*pre) != to_lower(*str))) {
            return false;
        }
        pre += 1;
        str += 1;
    }
    return true;
}


bool is_line_end(const char *buffer)
{
    if (buffer[0] != CR) {
        return false;
    }
    return buffer[1] == EOL;
}

void print_char(char c, FILE *stream)
{
    fprintf(stream, "%c", c);
}

void print_buffer(char *buf, size_t read_len, size_t start, FILE *stream)
{
    for (size_t i = start; i < read_len; ++i) {
        print_char(buf[i], stream);
    }
}

bool string_to_uint(const char *string_number, size_t *result)
{
    char *end_ptr = nullptr;
    errno = 0;
    size_t value = strtoull(string_number, &end_ptr, 10);
    if (*string_number == EOS || *end_ptr != EOS) {
        return false;
    }
    *result = value;
    return true;
}

bool tcp_addrrinfo(char *address, char *port, addrinfo **addr_result)
{
    addrinfo addr_hints{};
    memset(&addr_hints, 0, sizeof(addrinfo));
    addr_hints.ai_family = AF_INET;
    addr_hints.ai_socktype = SOCK_STREAM;
    addr_hints.ai_protocol = IPPROTO_TCP;
    addr_hints.ai_flags = 0;
    return getaddrinfo(address, port, &addr_hints, addr_result) == 0;
}

bool parse_timeout(char *string, size_t *timeout)
{
    return string_to_uint(string, timeout) && *timeout >= MIN_TIMEOUT_S;
}

bool is_valid_port(char *string)
{
    size_t port;
    return string_to_uint(string, &port) && port >= MIN_PORT && port <= MAX_PORT;
}

size_t read_data(char *buffer, size_t bytes, int sock)
{
    ssize_t read_len = 0;
    bool read_data = false;
    while (!read_data && !got_int_signal()) {
        errno = 0;
        read_len = read(sock, buffer, bytes);
        if (read_len == -1) {
            if (errno != EINTR) {
                close(sock);
                syserr("read socket");
            }
            else {
                continue;
            }
        }
        buffer[read_len] = EOS;
        read_data = true;
    }
    return got_int_signal() ? 0 : read_len;
}

void write_checked(const char *data, size_t bytes, int sock)
{
    ssize_t write_len = write(sock, data, bytes);

    if (write_len == -1) {
        syserr("write socket");
    }
}


bool get_local_addr(char *port, sockaddr_in *addr_result)
{
    size_t port_number = 0;
    bool result = string_to_uint(port, &port_number);
    addr_result->sin_family = AF_INET;
    addr_result->sin_addr.s_addr = htonl(INADDR_ANY);
    addr_result->sin_port = htons(port_number);
    return result;
}

void ntoh_buff(uint16_t *buffer, size_t len)
{
    for (size_t i = 0; i < len; ++i) {
        buffer[i] = ntohs(buffer[i]);
    }
}

void hton_buff(uint16_t *buffer, size_t len)
{
    for (size_t i = 0; i < len; ++i) {
        buffer[i] = htons(buffer[i]);
    }
}

void gettime(timespec *result)
{
    CHECK_ZERO_SYSERR(clock_gettime(CLOCK_REALTIME, result), "clock_gettime");
}

bool fits_time_range(const timespec &last_time, size_t range)
{
    timespec time{};
    gettime(&time);
    constexpr size_t nanos = 1E9;

    size_t diff_ns = (time.tv_sec - last_time.tv_sec) * nanos + (time.tv_nsec - last_time.tv_nsec);
    return diff_ns <= range * nanos;
}
