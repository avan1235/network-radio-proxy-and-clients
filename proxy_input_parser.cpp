#include "proxy_input_parser.h"

bool parse_elements(int argc, char **argv, parsed_elements_t *parsed)
{
    int c;
    char *meta = nullptr;
    char *timeout = nullptr;
    char *client_timeout = nullptr;
    memset(parsed, 0, sizeof(parsed_elements_t));

    while ((c = getopt(argc, argv, "h:r:p:P:m:t:B:T:")) != -1) {
        switch (c)
        {
            case 'h': parsed->host = optarg; break;
            case 'r': parsed->resource = optarg; break;
            case 'p': parsed->radio_port = optarg; break;
            case 'P': parsed->listen_port = optarg; break;
            case 'm': meta = optarg; break;
            case 't': timeout = optarg; break;
            case 'B': parsed->group_address = optarg; break;
            case 'T': client_timeout = optarg; break;
            case '?':
            default:
                return false;
        }
    }

    if (parsed->host == nullptr || parsed->resource == nullptr
        || parsed->radio_port == nullptr) {
        return false;
    }

    if (parsed->listen_port == nullptr
        && (parsed->group_address != nullptr || client_timeout != nullptr)) {
        return false;
    }

    if (timeout == nullptr) {
        parsed->timeout = PROXY_DEFAULT_TIMEOUT_S;
    }
    else if (!parse_timeout(timeout, &parsed->timeout)) {
        return false;
    }

    if (meta == nullptr) {
        parsed->meta = PROXY_DEFAULT_METADATA;
    }
    else if (strcmp(meta, YES_OPTION) == 0) {
        parsed->meta = true;
    }
    else if (strcmp(meta, NO_OPTION) == 0) {
        parsed->meta = false;
    }
    else {
        return false;
    }

    if (client_timeout == nullptr) {
        parsed->client_timeout = PROXY_DEFAULT_CLIENT_TIMEOUT_S;
    }
    else if (!parse_timeout(client_timeout, &parsed->client_timeout)) {
        return false;
    }

    return is_valid_port(parsed->radio_port) && (parsed->listen_port == nullptr || is_valid_port(parsed->listen_port));
}

void print_usage()
{
    DEBUG_PRINT(
            "Options:\n"
           "-h host [required] - host for audio stream\n"
           "-r resource [required] - resource on server name\n"
           "-p radio_port [required] - server radio_port for resource access\n"
           "-m yes|no [optional default=no] - request sending metadata from server\n"
           "-t timeout [optional default=%d] - server connection timeout seconds\n"
           "-P radio_port [optional] - server client listen radio_port\n"
           "-B multi [optional, valid when -P specified] - broadcast address\n"
           "-T timeout [optional, valid when -P specified, default=%d] - server client timeout seconds\n",
            PROXY_DEFAULT_TIMEOUT_S,
            PROXY_DEFAULT_CLIENT_TIMEOUT_S);
}