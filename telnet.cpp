#include "telnet.h"

/**
 * Write telnet specified in params data
 * @param data to be sent
 * @param bytes length of data to be sent
 * @param params with telnet specification
 */
static void write_telnet(const char *data, size_t bytes, telnet_t &params);

/**
 * Write telnet C style string that finishes with EOS
 * @param data with C string to be sent
 * @param params with telnet specification
 */
static void write_telnet_str(const char *data, telnet_t &params);

/**
 * Write telnet menu single position and mark currently played
 * radio proxy and the selected position on menu
 * @param data to be send as menu position
 * @param curr_pos currently sent position index
 * @param mark_pos index of position to be marked
 * @param selected true when position should be marked as current station
 * @param params with telnet specification
 */
static void write_telnet_pos_str(const char *data, size_t curr_pos, size_t mark_pos, bool selected, telnet_t &params);

/**
 * Sent to telnet configuration that allows to control
 * the program with arrows and Enter button
 * @param params with telnet specification
 */
static void set_telnet_conf(telnet_t &params);

/**
 * Clear telnet screen
 * @param params with telnet specification
 */
static void clear_telnet(telnet_t &params);

/**
 * Send telnet information about closing its
 * connection
 * @param params with telnet specification
 */
static void send_telnet_exit(telnet_t &params);

/**
 * Send telnet menu by sending its separated
 * menu positions and the current song title at the end
 * @param params with telnet specification
 */
static void send_telnet_menu(telnet_t &params);

/**
 * Check if buffer received from telnet has the specified command
 * @param buffer with data received
 * @param command to be compared with buffer
 * @param len length of command data (buffer has to have equal length)
 * @return true when the same command detected, otherwise false
 */
static bool is_command(const char *buffer, const uint8_t *command, size_t len);

/**
 * Extract title from buffer read from proxy by looking for
 * specified StreamTitle key and the value inside the '' chars
 * @param body to be parsed
 * @param body_len size of data in body in bytes
 * @return extracted string or empty string if no data in body
 */
static std::string extract_title(char *body, size_t body_len);

void keep_alive(telnet_t *params)
{
    constexpr static size_t sleep_us = KEEP_ALIVE_S * 1e6 + KEEP_ALIVE_US;
    uint16_t header[PROXY_HEADER_FIELDS];
    header[PROXY_HEADER_FIELD_TYPE] = KEEPALIVE;
    header[PROXY_HEADER_FIELD_LENGTH] = 0;
    hton_buff(header, PROXY_HEADER_FIELDS);

    while (!params->finished) {
        {
            std::scoped_lock lock(params->servers_mutex);
            if (params->selected != params->servers.end()) {
                DEBUG_PRINT("Sending KEEPALIVE to %s:%d\n",
                        inet_ntoa(params->selected->first.sin_addr),
                        params->selected->first.sin_port);
                // sendto to UDP socket so it is non-blocking
                // we also don't care about problems on sending here not to crash
                // because of some sending KEEPALIVE info
                sendto(params->proxy_sock, header, sizeof(header), 0,
                       (sockaddr *) &params->selected->first, sizeof(sockaddr_in));
            }
            else {
                DEBUG_PRINT("No server selected to send KEEPALIVE\n");
            }
        }
        if (usleep(sleep_us) == -1) {
            break;
        }
    }
    params->finished = true;
}

void check_alive_current(telnet_t *params)
{
    const size_t sleep_us = params->timeout * 1E6;
    while (!params->finished) {
        {
            params->servers_mutex.lock();
            if (!fits_time_range(params->last_conn, params->timeout)) {
                DEBUG_PRINT("Last connected proxy timeouted\n");
                if (params->selected != params->servers.end()) {
                    params->servers.erase(params->selected);
                    params->selected = params->servers.cend();
                }
                params->telnet_mutex.lock();
                params->current_title = EMPTY_TRACK_TITLE;
                params->telnet_mutex.unlock();
                params->servers_mutex.unlock();
                send_telnet_menu(*params);
            }
            else {
                params->servers_mutex.unlock();
            }
        }
        if (usleep(sleep_us) == -1) {
            break;
        }
    }
    params->finished = true;
}

void receive_proxy_data(telnet_t *params)
{
    ssize_t read_len;
    uint8_t buffer[MAX_SEND_DATA_LEN + PROXY_HEADER_SIZE + 1];
    sockaddr_in proxy_addr{};
    auto rcva_len = (socklen_t) sizeof(sockaddr_in);
    constexpr sockaddr_in_eq sock_eq;
    bool equal;

    while (!params->finished) {
        memset(buffer, 0, sizeof(buffer));
        read_len = recvfrom(params->proxy_sock, buffer, sizeof(buffer), 0,
                            (sockaddr *) &proxy_addr, &rcva_len);
        if (read_len <= PROXY_HEADER_SIZE) {
            DEBUG_PRINT("Got message of invalid length %ld. Skipping\n", read_len);
            continue;
        }
        auto *header = (uint16_t *) buffer;
        ntoh_buff(header, PROXY_HEADER_FIELDS);
        uint16_t body_len = header[PROXY_HEADER_FIELD_LENGTH];
        auto *body = (char *) (header + PROXY_HEADER_FIELDS);
        size_t safe_len = std::min((size_t) body_len, (size_t) (read_len - PROXY_HEADER_SIZE));
        if (safe_len != body_len) {
            DEBUG_PRINT("Length of data received not match declared in header\n");
        }
        {
            std::scoped_lock lock(params->servers_mutex);
            equal = sock_eq(proxy_addr, params->selected->first);
        }
        if (header[PROXY_HEADER_FIELD_TYPE] == IAM) {
            {
                std::scoped_lock lock(params->servers_mutex);
                params->servers[proxy_addr] = std::string(body);
                if (equal) {
                    gettime(&params->last_conn);
                }
            }
            send_telnet_menu(*params);
            continue;
        }
        if (!equal) {
            DEBUG_PRINT("Received %d from not selected server\n",
                    header[PROXY_HEADER_FIELD_TYPE]);
            continue;
        }
        {
            std::scoped_lock lock(params->servers_mutex);
            gettime(&params->last_conn);
        }
        if (header[PROXY_HEADER_FIELD_TYPE] == AUDIO) {
            print_buffer(body, safe_len, 0, stdout);
            continue;
        }
        if (header[PROXY_HEADER_FIELD_TYPE] == METADATA) {
            std::string extracted = extract_title(body, safe_len);
            if (extracted.length() > 0) {
                {
                    std::scoped_lock telnet_lock(params->telnet_mutex);
                    params->current_title = extracted;
                }
                send_telnet_menu(*params);
            }
            continue;
        }
        DEBUG_PRINT("Received header of type %d that will be ignored\n",
                    header[PROXY_HEADER_FIELD_TYPE]);
    }
}

void handle_telnet_connections(telnet_t &params)
{
    sockaddr_in client_addr{};
    socklen_t client_len = sizeof(client_addr);
    constexpr static uint8_t UP[] = {27, 91, 65};
    constexpr static uint8_t DOWN[] = {27, 91, 66};
    constexpr static uint8_t ENTER[] = {13, 0};
    constexpr sockaddr_in_eq sock_eq;

    uint16_t header[PROXY_HEADER_FIELDS];
    header[PROXY_HEADER_FIELD_TYPE] = DISCOVER;
    header[PROXY_HEADER_FIELD_LENGTH] = 0;
    hton_buff(header, PROXY_HEADER_FIELDS);

    while (!got_int_signal() && !params.finished) {
        DEBUG_PRINT("Wait on accept for client\n");
        params.msg_sock = accept(params.telnet_sock, (sockaddr *) &client_addr, &client_len);
        if (params.msg_sock < 0) {
            DEBUG_PRINT("Telnet accept error. Wait for another one\n");
            params.msg_sock = 0;
            continue;
        }
        DEBUG_PRINT("Client connected: %s:%d\n",
                inet_ntoa(client_addr.sin_addr), client_addr.sin_port);
        set_telnet_conf(params);

        size_t read_len;
        char buffer[MAX_COMMAND_SIZE];
        while (!params.finished) {
            DEBUG_PRINT("Send client current menu\n");
            send_telnet_menu(params);
            DEBUG_PRINT("Wait on client data send\n");

            memset(buffer,  0, sizeof(buffer));
            if ((read_len = read_data(buffer, MAX_COMMAND_SIZE, params.msg_sock)) == 0) {
                std::scoped_lock telnet_lock(params.telnet_mutex);
                int to_close = params.msg_sock;
                params.msg_sock = 0;
                CHECK_ZERO_SYSERR(close(to_close), "close");
                break;
            }

            switch (read_len) {
                case MOVE_COMMAND_LEN: {
                    ssize_t change = is_command(buffer, UP, MOVE_COMMAND_LEN) ? -1 :
                                     (is_command(buffer, DOWN, MOVE_COMMAND_LEN) ? 1 : 0);
                    DEBUG_PRINT("Will change position by %ld\n", change);
                    std::scoped_lock lock(params.servers_mutex);
                    size_t servers_size = params.servers.size();
                    if (params.menu_pos == 0 && change == -1) {
                        params.menu_pos = servers_size + TELNET_EXTRA_POS - 1;
                    }
                    else {
                        params.menu_pos += change;
                        params.menu_pos %= (servers_size + TELNET_EXTRA_POS);
                    }
                } break;
                case ENTER_COMMAND_LEN: {
                    if (is_command(buffer, ENTER, ENTER_COMMAND_LEN)) {
                        DEBUG_PRINT("Got ENTER command\n");
                        std::scoped_lock lock(params.servers_mutex);
                        size_t servers_size = params.servers.size();
                        if (params.menu_pos == servers_size + 1) {
                            DEBUG_PRINT("\"Finish\" option selected\n");
                            send_telnet_exit(params);
                            params.finished = true;
                        }
                        else if (params.menu_pos == 0) {
                            DEBUG_PRINT("\"Discover\" option selected\n");
                            sendto(params.proxy_sock, header, sizeof(header), 0,
                                   (sockaddr *) &params.proxy_addr, sizeof(sockaddr_in));
                        }
                        else {
                            DEBUG_PRINT("Selected %zu item on radio list\n", params.menu_pos);
                            sendto(params.proxy_sock, header, sizeof(header), 0,
                                   (sockaddr *) &params.proxy_addr, sizeof(sockaddr_in));
                            size_t pos = 1;
                            for (auto it = params.servers.begin(); it != params.servers.cend(); ++it) {
                                if (pos == params.menu_pos) {
                                    if (!sock_eq(it->first, params.selected->first)) {
                                        std::scoped_lock telnet_lock(params.telnet_mutex);
                                        params.current_title = EMPTY_TRACK_TITLE;
                                    }
                                    gettime(&params.last_conn);
                                    params.selected = it;
                                    break;
                                }
                                pos += 1;
                            }
                        }
                    }
                    else {
                        DEBUG_PRINT("Invalid command of length equal to ENTER_COMMAND_LEN\n");
                    }
                } break;
                default: {
                    DEBUG_PRINT("Received not supported command of length: %lu with bytes:\n", read_len);
                    for (size_t i = 0; i < read_len; ++i) {
                        DEBUG_PRINT("%d ", buffer[i]);
                    }
                    DEBUG_PRINT_CHAR(EOL);
                }
            }
        }
    }
    params.finished = true;
}

static void write_telnet(const char *data, size_t bytes, telnet_t &params)
{
    int sock = params.msg_sock;
    if (sock == 0) {
        DEBUG_PRINT("Empty telnet sock passed for writing - ignoring\n");
        return;
    }
    errno = 0;
    ssize_t write_len = write(sock, data, bytes);

    if (write_len == -1) {
        DEBUG_PRINT("Received error %d when write to telnet socket\n", errno);
    }
}

static void write_telnet_str(const char *data, telnet_t &params)
{
    write_telnet(data, strlen(data), params);
}

static void write_telnet_pos_str(const char *data, size_t curr_pos, size_t mark_pos, bool selected, telnet_t &params)
{
    if (curr_pos == mark_pos) {
        write_telnet_str(TELNET_CURSOR, params);
    }
    write_telnet_str(data, params);
    if (selected) {
        write_telnet_str(TELNET_SELECTED, params);
    }
    write_telnet(CRLF, CRLF_LEN, params);
}

static void set_telnet_conf(telnet_t &params)
{
    std::scoped_lock telnet_lock(params.telnet_mutex);
    static const std::string conf = "\377\375\042\377\373\001";
    write_telnet_str(conf.c_str(), params);
}

static void clear_telnet(telnet_t &params)
{
    static const std::string flush = "\u001Bc";
    write_telnet_str(flush.c_str(), params);
}

static void send_telnet_exit(telnet_t &params)
{
    std::scoped_lock telnet_lock(params.telnet_mutex);
    clear_telnet(params);
    write_telnet_str(TELNET_EXIT_MSG, params);
    write_telnet_str(CRLF, params);
}

static void send_telnet_menu(telnet_t &params)
{
    std::scoped_lock telnet_lock(params.telnet_mutex);
    clear_telnet(params);
    std::scoped_lock lock(params.servers_mutex);
    params.menu_pos %= (params.servers.size() + TELNET_EXTRA_POS);
    size_t curr_pos = 0;
    write_telnet_pos_str(TELNET_SEARCH_MSG, curr_pos++, params.menu_pos, false, params);
    for (auto it = params.servers.begin(); it != params.servers.cend(); ++it) {
        write_telnet_pos_str(it->second.c_str(), curr_pos++, params.menu_pos, it == params.selected, params);
    }
    write_telnet_pos_str(TELNET_END_MSG, curr_pos, params.menu_pos, false, params);
    if (params.current_title.length() > 0) {
        write_telnet_str(PLAYING_MSG, params);
        write_telnet_str(params.current_title.c_str(), params);
        write_telnet_str(CRLF, params);
    }
}

static bool is_command(const char *buffer, const uint8_t *command, size_t len)
{
    for (size_t i = 0; i < len; ++i) {
        if (buffer[i] != command[i]) {
            return false;
        }
    }
    return true;
}

static std::string extract_title(char *body, size_t body_len)
{
    static const std::string TITLE_NAME = "StreamTitle='";
    static const std::string DELIMITER = "';";
    body[body_len] = EOS;
    std::string result;
    std::string search_in(body);
    size_t pos = search_in.find(TITLE_NAME);
    while (pos != std::string::npos) {
        size_t finish_pos = search_in.find(DELIMITER, pos + TITLE_NAME.length());
        if (finish_pos == std::string::npos) {
            finish_pos = search_in.length();
        }
        size_t start = pos + TITLE_NAME.length();
        size_t len = finish_pos - start;
        std::string found = search_in.substr(start, len);
        if (found.length() > 0) {
            result = found;
        }
        pos = search_in.find(TITLE_NAME, finish_pos);
    }
    return result;
}