#ifndef RADIO_PROXY_TELNET_H
#define RADIO_PROXY_TELNET_H

#include <cstdio>
#include <arpa/inet.h>
#include <mutex>
#include <algorithm>

#include "util.h"

#define TELNET_SEARCH_MSG "Szukaj pośrednika"
#define TELNET_END_MSG "Koniec"
#define TELNET_EXIT_MSG "Zamykanie klienta..."
#define PLAYING_MSG "Tytuł: "
#define TELNET_SELECTED " *"
#define TELNET_CURSOR "> "
#define TELNET_EXTRA_POS 2

#define MAX_COMMAND_SIZE 256
#define MOVE_COMMAND_LEN 3
#define ENTER_COMMAND_LEN 2

#define KEEP_ALIVE_S 3
#define KEEP_ALIVE_US 5*1e5

#define RECV_TIMEOUT_S 1

struct telnet_handler_params {
    servers_map &servers;
    std::mutex servers_mutex;
    size_t menu_pos;
    servers_map::const_iterator &selected;
    std::mutex telnet_mutex;
    std::string current_title;
    bool finished;
    int msg_sock;
    const int telnet_sock;
    const int proxy_sock;
    const size_t timeout;
    const sockaddr_in proxy_addr;
    timespec last_conn;
};

typedef telnet_handler_params telnet_t;

/**
 * Thread to check if currently selected radio
 * is still alive by comparing its last send time
 * with current time. When times out the server
 * is removed from list and radio gets disabled
 * @param params of connections configuration
 */
void check_alive_current(telnet_t *params);

/**
 * Thread to send KEEPALIVE message to currently
 * selected radio proxy in regular time ranges
 * @param params of connections configuration
 */
void keep_alive(telnet_t *params);

/**
 * Receive proxy data from on proxy socket, validate it and update
 * current state based on data received. When the data received is
 * of type IAM new proxy server is added to list of servers.
 * When the type is METADATA or AUDIO the sender is checked if
 * matches currently selected radio proxy and if valid the data is
 * passed to telnet or to output
 * @param params of connections configuration
 */
void receive_proxy_data(telnet_t *params);

/**
 * Handle telnet single connection and display menu data
 * when user would like to change the menu option or select
 * currently played radio proxy source
 * @param params of connections configuration
 */
void handle_telnet_connections(telnet_t &params);

#endif //RADIO_PROXY_TELNET_H
