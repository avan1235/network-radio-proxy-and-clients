#include "signal_handlers.h"

static bool got_int = false;

bool got_int_signal()
{
    return got_int;
}

void handle_sigint(int sig)
{
    got_int = (sig == SIGINT);
    DEBUG_PRINT("Handle signal: %d\n", sig);
}

void signal_on(int sig, void (*handler)(int))
{
    struct sigaction action{};
    action.sa_handler = handler;
    action.sa_flags = 0;
    sigemptyset(&action.sa_mask);
    sigaction(sig, &action, nullptr);
}