#ifndef RADIO_PROXY_REQUEST_H
#define RADIO_PROXY_REQUEST_H

#include <cstring>
#include <string>
#include <zconf.h>

#include "err.h"
#include "proxy_input_parser.h"
#include "constants.h"
#include "util.h"

#define GET "GET "
#define HTTP_1_0 " HTTP/1.0"
#define HOST_KEY "Host: "
#define USER_AGENT_KEY "User-Agent: "
#define USER_AGENT_VALUE "radio-proxy"
#define METADATA_KEY "Icy-MetaData: "

#define REQUEST_ELEMENTS 14

/**
 * Send request for shoutcast data from radio server
 * @param sock to send request data to
 * @param parsed parameters of request from parsed user input
 */
void send_request(int sock, parsed_elements_t *parsed);


#endif //RADIO_PROXY_REQUEST_H
