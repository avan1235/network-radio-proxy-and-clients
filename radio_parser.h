#ifndef RADIO_PROXY_RADIO_PARSER_H
#define RADIO_PROXY_RADIO_PARSER_H

#include <arpa/inet.h>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <functional>

#include "constants.h"
#include "util.h"
#include "request.h"
#include "signal_handlers.h"

#define POSSIBLE_VALID_STATUS_LINES 3
#define MIN_VALID_STATUS_LINE_LEN 12
#define ICY_HEADER_DATA_MAX_LEN NAME_BUFFER_SIZE // must be <= min(RESPONSE_BUFFER_SIZE, NAME_BUFFER_SIZE)

#define META_INT_KEY "icy-metaint:"
#define META_NAME_KEY "icy-name:"

#define META_INT_KEY_LEN 12 // length of META_INT_KEY
#define META_NAME_KEY_LEN 9 // length of META_NAME_KEY

#define META_INT_MULTIPLY 16

struct proxy_params {
    const int proxy_sock;
    const int radio_sock;
    const size_t clients_timeout;
    char *radio_name;
    clients_map clients_times;
    std::mutex mutex;
};

typedef proxy_params proxy_t;

/**
 * Parse first line of server response for status code value checking
 * @param buf where the FULL status line should be loaded
 * @param read_len length of data read to buf
 * @param buf_pos current position in buf when line starts
 * @return true when OK code received, otherwise false
 */
bool parse_status_line(const char *buf, size_t read_len, size_t *buf_pos);

/**
 * Read header content from server response, parse the value of data interval
 * between metadata and give the information if such field appeared in header.
 * @param buf with already loaded data
 * @param read_len of data loaded to buffer
 * @param buf_pos current position in buffer
 * @param meta_int pointer to set the eventually found meta int value
 * @param radio_name pointer to set the received radio name from header
 * @param sock to read next buffer data from
 * @return true when found meta int value in header, otherwise false
 */
bool read_header(char *buf, size_t *read_len, size_t *buf_pos, size_t *meta_int, char *radio_name, int sock);

/**
 * Read content send by Shoutcast server to client and read metadata if
 * it is a part of content. The sound data is send to the data_stream while
 * metadata to the meta_stream
 * @param buf where is data read from server
 * @param read_len of already read data to buffer
 * @param pos current position in buffer
 * @param has_meta true when meta length byte should be check after every
 * meta_int bytes
 * @param meta_int constant length of sound data between metadata intervals
 * @param data_stream to print data values to
 * @param meta_stream to print meta values to
 * @param sock to read next data from
 */
void local_read_response_body(char *buf, size_t read_len, size_t pos, bool has_meta, size_t meta_int,
                              FILE *data_stream, FILE *meta_stream, int sock);

/**
 * Handle connections of clients sending KEEPALIVE and DISCOVER
 * communicates to the server or it's group address. Respond to clients
 * with IAM communicate and mark the clients as last time active on
 * the time specified by the time of communicate time arrived
 * @param params with proxy configuration
 */
void handle_connection(proxy_t *params);

/**
 * Served the body read from radio to connected clients list in separate
 * parts that are the AUDIO and the METADATA parts of data
 * @param buf where some part of data is already loaded
 * @param read_len size of data loaded to buf
 * @param pos current position of data read in buf
 * @param has_meta mark if the metadata is going to be send by radio
 * @param meta_int interval in bytes of data between metadata informations
 * @param params with proxy configuration
 */
void serve_response_body(char *buf, size_t read_len, size_t pos, bool has_meta, size_t meta_int, proxy_t &params);

#endif //RADIO_PROXY_RADIO_PARSER_H